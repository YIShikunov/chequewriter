﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberToWords
{
    public class NumberToWordsConverterEnglish : NumberToWordsCoverter
    {
        protected override string CurrencyName { get; } = "DOLLAR";
        protected override string PluralSuffix { get; } = "S";
        protected override string DecimalName { get; } = "CENT";
        protected override string DecimalConnector { get; } = "AND";
        protected override string QuantifierConnector { get; } = "and";
        protected override char QuantifierMarker { get; } = '|';
        protected override string ValueGroupsSeparator { get; } = ", ";
        protected override string ValueSeparator { get; } = " ";


        protected override Dictionary<List<int>, HashSet<PolynomialNumberIdentifier>> ClusteredNumberNames { get; } = new Dictionary<List<int>, HashSet<PolynomialNumberIdentifier>>()
        {
            { new List<int>(){1}, new HashSet<PolynomialNumberIdentifier>(){
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 2 } }, "twenty"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 3 } }, "thirty"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 4 } }, "fourty"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 5 } }, "fifty"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 6 } }, "sixty"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 7 } }, "seventy"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 8 } }, "eighty"),
                new PolynomialNumberIdentifier(new Dictionary<int, int>(){ {1, 9 } }, "ninety"),
             }},
            { new List<int>() {0, 1 }, new HashSet<PolynomialNumberIdentifier>(){
                new PolynomialNumberIdentifier(10, "ten"),
                new PolynomialNumberIdentifier(11, "eleven"),
                new PolynomialNumberIdentifier(12, "twelve"),
                new PolynomialNumberIdentifier(13, "thirteen"),
                new PolynomialNumberIdentifier(14, "fourteen"),
                new PolynomialNumberIdentifier(15, "fifteen"),
                new PolynomialNumberIdentifier(16, "sixteen"),
                new PolynomialNumberIdentifier(17, "seventeen"),
                new PolynomialNumberIdentifier(18, "eighteen"),
                new PolynomialNumberIdentifier(19, "nineteen"),
            } },

        };

        protected override Dictionary<int, string> QuantifierNames { get; } = new Dictionary<int, string>()
        {
            {2, "hundred" }
        };


        protected override Dictionary<int, string> DigitNames { get; } = new Dictionary<int, string>()
        {
            {1, "one" },
            {2, "two" },
            {3, "three" },
            {4, "four" },
            {5, "five" },
            {6, "six" },
            {7, "seven" },
            {8, "eight" },
            {9, "nine" },
        };

        protected override Dictionary<int, string> IntegerGroupQuantifierNames { get; } = new Dictionary<int, string>()
        {
            {0, "" },
            {1, "thousand" },
            {2, "million" },
            {3, "billion" }
        };

    }
}
