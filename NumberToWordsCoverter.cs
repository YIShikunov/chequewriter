﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics.CodeAnalysis;

namespace NumberToWords
{

    public struct PolynomialNumberIdentifier : IEquatable<PolynomialNumberIdentifier>
    {
        public readonly Dictionary<int, int> identifier;
        public string name;

        public PolynomialNumberIdentifier(int number, string name = "")
        {
            identifier = new Dictionary<int, int>();
            this.name = name;
            var poly = NumberToWordsCoverter.ConvertToBase10Polynomial(number);
            for (int i = 0; i < poly.Count; i++)
            {
                identifier.Add(i, poly[i]);
            }
        }

        public PolynomialNumberIdentifier(List<int> poly, string name = "")
        {
            identifier = new Dictionary<int, int>();
            this.name = name;
            for (int i = 0; i < poly.Count; i++)
            {
                identifier.Add(i, poly[i]);
            }
        }

        public PolynomialNumberIdentifier(Dictionary<int, int> id, string name = "")
        {
            identifier = id;
            this.name = name;
        }

        public bool Equals(PolynomialNumberIdentifier other)
        {
            foreach (var id in identifier)
            {
                var hasKey = other.identifier.TryGetValue(id.Key, out var otherValue);
                if (!hasKey || otherValue != id.Value)
                {
                    return false;
                }
            }
            return true;
        }
        public override int GetHashCode()
        {
            int ret = 0;
            foreach (var id in identifier)
            {
                ret ^= id.GetHashCode();
            }
            return ret;
        }
    }

    public abstract class NumberToWordsCoverter
    {
        //Both bounds are expected to fail when equal
        private const decimal MAX_VALUE_BOUND = 2000000000;
        private const decimal MIN_VALUE_BOUND = 0;

        protected virtual string CurrencyName { get; }
        protected virtual string PluralSuffix { get; }
        protected virtual string DecimalName { get; }
        protected virtual string DecimalConnector { get; }
        protected virtual string QuantifierConnector { get; }
        protected virtual char QuantifierMarker { get; }
        protected virtual string ValueGroupsSeparator { get; }
        protected virtual string ValueSeparator { get; }

        //Defines how many digits are defined per group quantifier
        protected virtual int IntegerGroupPower { get; } = 3;

        protected virtual Dictionary<List<int>, HashSet<PolynomialNumberIdentifier>> ClusteredNumberNames { get; }

        protected virtual Dictionary<int, string> QuantifierNames { get; }

        protected virtual Dictionary<int, string> IntegerGroupQuantifierNames { get; }

        protected virtual Dictionary<int, string> DigitNames { get; }

        //Split Sanitize and Convert to make it easier to unit test

        /// <summary>
        /// Transcribes the input number into a string of words describing this number
        /// </summary>
        /// <param name="number">Input decimal number. Expected to have 2 decimal places</param>
        /// <returns></returns>
        public virtual string Convert(decimal number)
        {
            var integerPart = (int)Math.Truncate(number);
            var decimalPart = (int)((number % 1) * 100);
            var integerPoly = ConvertToBase10Polynomial(integerPart);
            var decimalPoly = ConvertToBase10Polynomial(decimalPart);
            string decimalValue = ProcessGroup(decimalPoly);
            Stack<string> integerGroupValues = new Stack<string>();
            for (int i = 0; i < integerPoly.Count; i += IntegerGroupPower)
            {
                List<int> groupPoly;
                if (i + IntegerGroupPower > integerPoly.Count)
                {
                    groupPoly = integerPoly.GetRange(i, integerPoly.Count - i);
                } else
                {
                    groupPoly = integerPoly.GetRange(i, IntegerGroupPower);
                }
                if (groupPoly.All((x) => { return x == 0; }))
                {
                    continue;
                }
                string groupValue = ProcessGroup(groupPoly);
                var currentGroupPower = i / IntegerGroupPower;
                groupValue += ValueSeparator + IntegerGroupQuantifierNames[currentGroupPower];
                integerGroupValues.Push(groupValue);
            }
            var integerValue = String.Join(ValueGroupsSeparator, integerGroupValues);
            var intergerName = integerPoly.Count == 1 && integerPoly[0] == 1 ? CurrencyName : CurrencyName + PluralSuffix;
            var decimalName = decimalPoly.Count == 1 && decimalPoly[0] == 1 ? DecimalName : DecimalName + PluralSuffix;
            string ret;
            //Remove unnecessary separators
            if (integerPoly.Count == 0)
            {
                ret = String.Join(ValueSeparator, decimalValue, decimalName);
            }
            else if (decimalPoly.Count == 0)
            {
                ret = String.Join(ValueSeparator, integerValue, intergerName);
            }
            else
            {
                ret = String.Join(ValueSeparator, integerValue, intergerName, DecimalConnector, decimalValue, decimalName);
            }
            //remove repeated whitespaces
            var split = ret.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries);
            return String.Join(ValueSeparator, split);
        }

        protected virtual string ProcessGroup(List<int> poly)
        {
            SortedDictionary<int, string> retHirerachy = new SortedDictionary<int, string>();
            
            foreach (var quantifier in QuantifierNames)
            {
                if (poly.Count > quantifier.Key)
                {
                    int digit = poly[quantifier.Key];
                    if (digit > 0) 
                    {
                        retHirerachy[quantifier.Key] = String.Join(ValueSeparator, ProcessDigits(digit), quantifier.Value, QuantifierMarker); ;
                    }
                }
            }

            foreach (var cluster in ClusteredNumberNames)
            {
                var dict = new Dictionary<int, int>();
                foreach (var power in cluster.Key)
                {
                    if (retHirerachy.ContainsKey(power) || poly.Count <= power)
                    {
                        continue;
                    }
                    dict.Add(power, poly[power]);
                }
                var clusterCandidate = new PolynomialNumberIdentifier(dict);
                if (cluster.Value.TryGetValue(clusterCandidate, out var numberId))
                {
                    //define every power, so we can know when to quit
                    foreach(var power in numberId.identifier)
                    {
                        if (!retHirerachy.ContainsKey(power.Key))
                        {
                            retHirerachy[power.Key] = "";
                        }
                        else
                        {
                            throw new FormatException($"Double definition of a poly term {power.Value}. Term at position {power.Key} was already defined as \"{retHirerachy[power.Key]}\"");
                        }
                    }
                    //define one of the powers as an actual term
                    retHirerachy[numberId.identifier.First().Key] = numberId.name;
                }
            }

            if (retHirerachy.Count < poly.Count)
            {
                for (int i = poly.Count - 1; i >= 0; i--)
                {
                    if (!retHirerachy.ContainsKey(i) && poly[i] > 0)
                    {
                        retHirerachy[i] = ProcessDigits(poly[i]);
                    }
                }
            }
            //Removes all trailing whitespaces, and replaces quantifier marker with a connecting string only when there is another term to connect to.
            var ret = string.Join(ValueSeparator, retHirerachy.Values.Reverse()).Trim();
            var split = ret.Split(QuantifierMarker, StringSplitOptions.RemoveEmptyEntries);
            return String.Join(QuantifierConnector, split);


        }
        /// <exception cref="System.ArgumentException">Thrown when digit is not a defined in a language</exception>
        protected string ProcessDigits(int digit)
        {
            if (!DigitNames.TryGetValue(digit, out var digitName))
            {
                throw new ArgumentException($"{digit} was not defined");
            }
            return digitName;
        }

        /// <summary>
        /// Sanitizes the input 
        /// </summary>
        /// <param name="input">Input String. Should be a floating point number</param>
        /// <returns></returns>
        public bool TrySanitizeInput(string input, out decimal output)
        {
            var success = Decimal.TryParse(input, System.Globalization.NumberStyles.Number, System.Globalization.NumberFormatInfo.InvariantInfo, out var parsedNumber);
            if (!success)
            {
                output = Decimal.Zero;
                return false;
            }
            if (parsedNumber >= MAX_VALUE_BOUND || parsedNumber <= MIN_VALUE_BOUND)
            {
                output = Decimal.Zero;
                return false;
            }
            //Truncate the precision to exactly 2 digits
            output = Decimal.Truncate(parsedNumber * 100) / 100;
            return true;
        }
        public static List<int> ConvertToBase10Polynomial(int number)
        {
            List<int> ret = new List<int>();
            while (number > 0)
            {
                ret.Add(number % 10);
                number /= 10;
            }
            return ret;

        }
    }
}
