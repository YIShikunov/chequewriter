﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToWords;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberToWords.Tests
{
    [TestClass()]
    public class NumbersToWordsConverterEnglishTests
    {
        [TestMethod()]
        public void ConvertTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.Convert((decimal)1357256.32);
            Assert.AreEqual("one million, three hundred and fifty seven thousand, two hundred and fifty six DOLLARS AND thirty two CENTS", result);
        }
        [TestMethod()]
        public void ConvertZeroDecimalsTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.Convert((decimal)0.69);
            Assert.AreEqual("sixty nine CENTS", result);
        }
        [TestMethod()]
        public void ConvertWithoutDecimalsTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.Convert((decimal)1191);
            Assert.AreEqual("one thousand, one hundred and ninety one DOLLARS", result);
        }
        [TestMethod()]
        public void ConvertEmptyGroupTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.Convert((decimal)1000000000);
            Assert.AreEqual("one billion DOLLARS", result);
            result = converter.Convert((decimal)1000000);
            Assert.AreEqual("one million DOLLARS", result);
        }
        [TestMethod()]
        public void ConvertSingularCurrencyTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.Convert((decimal)1.01);
            Assert.AreEqual("one DOLLAR AND one CENT", result);
        }
        [TestMethod()]
        public void ConvertDecimalTensDigitTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.Convert((decimal)1.1);
            Assert.AreEqual("one DOLLAR AND ten CENTS", result);
        }
        [TestMethod()]
        public void SanitizeInputTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("1357256.32", out var output);
            Assert.IsTrue(result);
            Assert.AreEqual((decimal)1357256.32, output);
        }
        [TestMethod()]
        public void SanitizeInputMorePrecisionTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("1357256.3298", out var output);
            Assert.IsTrue(result);
            Assert.AreEqual((decimal)1357256.32, output);
        }
        [TestMethod()]
        public void SanitizeInputNegativeTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("-1357256.32", out var output);
            Assert.IsFalse(result);
            Assert.AreEqual(0, output);
        }
        [TestMethod()]
        public void SanitizeInputHugeTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("2000000000", out var output);
            Assert.IsFalse(result);
            Assert.AreEqual(0, output);
        }
        [TestMethod()]
        public void SanitizeInputHugeDecimalTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("2000000000.01", out var output);
            Assert.IsFalse(result);
            Assert.AreEqual(0, output);
        }
        [TestMethod()]
        public void SanitizeInputHuge2BTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("1999999999.99", out var output);
            Assert.IsTrue(result);
            Assert.AreEqual((decimal)1999999999.99, output);
        }
        [TestMethod()]
        public void SanitizeInputZeroTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("0.00", out var output);
            Assert.IsFalse(result);
            Assert.AreEqual(0, output);
        }
        [TestMethod()]
        public void SanitizeInputZeroDecimalsTest()
        {
            var converter = new NumberToWords.NumberToWordsConverterEnglish();
            var result = converter.TrySanitizeInput("0.69", out var output);
            Assert.IsTrue(result);
            Assert.AreEqual((decimal)0.69, output);
        }
        
    }
}