﻿using System;
using System.Collections.Generic;

namespace NumberToWords
{
    class Program
    {
        static void Main(string[] args)
        {
            var converter = new NumberToWordsConverterEnglish();
            Console.WriteLine("Enter a single floating point number");
            decimal sanitized;
            while (!converter.TrySanitizeInput(Console.ReadLine() , out sanitized))
            {
                Console.WriteLine("Failed to parse input, please try again:");
            }
            
            Console.WriteLine(converter.Convert(sanitized));

        }
    }
}
